# Refactoring exercise
Please review the following code snippet. Assume that all referenced assemblies have
been properly included.
The code is used to log different messages throughout an application. We want the ability
to be able to log to a text file, the console and/or the database. Messages can be marked
as message, warning or error. We also want the ability to selectively be able to choose
what gets logged, such as to be able to log only errors or only errors and warnings.

## 1.
- lineas 52 al 58, no se se hace un control apropiado de la coneccion a la base de datos.
- lineas 84 al 94, no se crea una variable 'l' la cual se concatena con fecha y el mensaje, pero no se usa para registrar algun evento. es una variable que no se usa.
- lineas 101 al 108, uso exesivo de estructuras de condicion (if en general) 
- si se quisiera cambiar la configuracion, se tendria que modificar el código y luego recompilar la aplicación (cada vez que algun valor cambiara). 
## 2.
- En general el manejo de los errors implementan el pattron de diseño "Chain of Responsibility" por lo cual lo ideal es implementar un nuevo receptor que se encarge de manejar la inserción de los logs en una base de datos.

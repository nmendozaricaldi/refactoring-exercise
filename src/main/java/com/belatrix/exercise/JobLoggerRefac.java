package com.belatrix.exercise;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class JobLoggerRefac {

	private static Logger LOGGER = null;

	static {
		InputStream stream = JobLoggerRefac.class.getClassLoader().getResourceAsStream("logging.properties");
		try {
			LogManager.getLogManager().readConfiguration(stream);
			LOGGER = Logger.getLogger(JobLoggerRefac.class.getName());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void LogMessage(String messageText,
			Level level) throws Exception {
		messageText.trim();
		if (messageText == null || messageText.length() == 0) {
			return;
		}
		if (level == null) {
			throw new Exception("Error, Warning or Message must be specified");
		}
		LOGGER.log(level, messageText);
	}
}

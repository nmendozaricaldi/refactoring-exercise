package com.belatrix.exercise.logging;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Handler;
import java.util.logging.LogManager;
import java.util.logging.LogRecord;

public class JDBCHandler extends Handler {

	protected String databaseURL = "jdbc:odbc:myDB";
	/**
	 * User to connect as for default connection handling
	 */
	protected String databaseUser = "";

	/**
	 * User to use for default connection handling
	 */
	protected String databasePassword = "";

	protected Connection connection = null;

	protected String sqlStatement = "";

	private void configure() {
		LogManager localLogManager = LogManager.getLogManager();
		String str = getClass().getName();
		String _url = localLogManager.getProperty(str + ".url");
		if (_url != null) {
			this.databaseURL = _url;
		}
		String _user = localLogManager.getProperty(str + ".user");
		if (_user != null) {
			this.databaseUser = _user;
		}
		String _pass = localLogManager.getProperty(str + ".password");
		if (_pass != null) {
			this.databasePassword = _pass;
		}
		JDBCFormatter formatter = new JDBCFormatter();
		setFormatter(formatter);
	}

	public JDBCHandler() {
		configure();
	}

	@Override
	public void publish(LogRecord record) {
		if (!isLoggable(record)) {
			return;
		}
		flushBuffer(record);
	}

	public void flushBuffer(LogRecord record) {
		String sql = null;
		try {
			sql = getLogStatement(record);
			execute(sql);
		} catch (SQLException e) {
			reportError("Failed to excute sql " + sql, e, 5);
		} finally {
			// removes.add(logEvent);
		}
	}

	protected String getLogStatement(LogRecord event) {
		String str = getFormatter().format(event);
		return str;
	}

	protected void execute(String sql) throws SQLException {
		Connection con = null;
		Statement stmt = null;
		try {
			con = getConnection();
			stmt = con.createStatement();
			stmt.executeUpdate(sql);
		} finally {
			if (stmt != null) {
				stmt.close();
			}
			closeConnection(con);
		}
	}

	protected Connection getConnection() throws SQLException {
		if (!DriverManager.getDrivers().hasMoreElements())
			setDriver("sun.jdbc.odbc.JdbcOdbcDriver");

		if (connection == null) {
			connection = DriverManager.getConnection(databaseURL, databaseUser, databasePassword);
		}

		return connection;
	}

	public void setDriver(String driverClass) {
		try {
			Class.forName(driverClass);
		} catch (Exception e) {
			reportError(null, e, 5);
		}
	}

	protected void closeConnection(Connection con) {
	}

	@Override
	public void flush() {
	}

	@Override
	public void close() throws SecurityException {
	}
}

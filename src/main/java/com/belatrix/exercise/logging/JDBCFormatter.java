package com.belatrix.exercise.logging;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
import java.util.logging.LogRecord;
import java.util.logging.SimpleFormatter;

public class JDBCFormatter extends SimpleFormatter {
	private static final String format = "INSERT INTO LOGS(date_log,date_string,level,message) VALUES  (CURRENT_TIMESTAMP, '%1$tF %1$tT', '%4$-7s' ,'%5$s')";
	private final Date dat = new Date();

	public synchronized String format(LogRecord paramLogRecord) {
		this.dat.setTime(paramLogRecord.getMillis());
		String str1;
		if (paramLogRecord.getSourceClassName() != null) {
			str1 = paramLogRecord.getSourceClassName();
			if (paramLogRecord.getSourceMethodName() != null) {
				str1 = str1 + " " + paramLogRecord.getSourceMethodName();
			}
		} else {
			str1 = paramLogRecord.getLoggerName();
		}
		String str2 = formatMessage(paramLogRecord);
		String str3 = "";
		if (paramLogRecord.getThrown() != null) {
			StringWriter localStringWriter = new StringWriter();
			PrintWriter localPrintWriter = new PrintWriter(localStringWriter);
			localPrintWriter.println();
			paramLogRecord.getThrown().printStackTrace(localPrintWriter);
			localPrintWriter.close();
			str3 = localStringWriter.toString();
		}
		return String.format(format, new Object[] { this.dat, str1, paramLogRecord.getLoggerName(),
				paramLogRecord.getLevel().getLocalizedName(), str2, str3 });
	}
}

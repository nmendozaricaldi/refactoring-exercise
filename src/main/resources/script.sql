CREATE TABLE IF NOT EXISTS logs
(
    date_log time with time zone,
    date_string character varying,
    level character varying,
    message character varying,
    id bigint auto_increment
)
package com.belatrix.exercise;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.LogManager;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class JobLoggerRefacTest {

	// DataBase Conf
	protected Connection connection = null;
	protected static String databaseURL;
	protected static String databaseUser;
	protected static String databasePassword;

	// load database configuration
	@BeforeClass
	public static void onceExecutedBeforeAll() {
		InputStream stream = JobLoggerRefac.class.getClassLoader().getResourceAsStream("logging.properties");
		try {
			LogManager.getLogManager().readConfiguration(stream);
		} catch (IOException e) {
			e.printStackTrace();
		}
		LogManager localLogManager = LogManager.getLogManager();
		String str = "com.belatrix.exercise.logging.JDBCHandler";
		String _url = localLogManager.getProperty(str + ".url");
		if (_url != null) {
			databaseURL = _url;
		}
		assertEquals(databaseURL, "jdbc:h2:mem:example;DB_CLOSE_DELAY=-1");
	}

	// create database connection
	@Before
	public void executedBeforeEach() throws SQLException {
		connection = DriverManager.getConnection(databaseURL, databaseUser, databasePassword);
		// delete if exist and create table
		Statement st = connection.createStatement();
		st.execute(
				"CREATE TABLE IF NOT EXISTS logs (date_log time with time zone, date_string character varying, level character varying, message character varying, id bigint auto_increment)");
	}

	// Test saving Log INFO
	@Test
	public void testLogMessageIsSent() throws Exception {
		int countBefore = countLogs();
		JobLoggerRefac.LogMessage("Test message", Level.INFO);
		assertEquals(countBefore + 1, countLogs());
		connection.close();
	}

	// Test saving Log INFO AND WARNING
	@Test
	public void testLogMessageWarningAndInfoIsSent() throws Exception {
		int countBefore = countLogs();
		// saving Log
		JobLoggerRefac.LogMessage("Test message Info", Level.INFO);
		JobLoggerRefac.LogMessage("Test message Warning", Level.WARNING);
		assertEquals(countBefore + 2, countLogs());
		connection.close();
	}

	private int countLogs() {
		int count = 0;
		Statement stmt = null;
		try {
			stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT count(*) from logs");
			while (rs.next()) {
				count = rs.getInt(1);
			}
		} catch (Exception e) {
			System.out.print(e);
			fail();
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					System.out.print(e);
				}
			}
		}
		return count;
	}

}
